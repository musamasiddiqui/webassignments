<?php
    $er = 0;
    $time = 0;
    $du = 0;
    $result = 0;
    $mul = 0;
    if(isset($_POST['submit'])){
        $er = $_POST['eRelease'];
        $time = $_POST['time'];
        $du = $_POST['duMedium'];
        $mul = $er*($time*$time);
        $result = pow((($mul/$du)),1/5);
        
    }

?>
<html>
<head>
    <title>Q4</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script></head>
    <body>
    <div class="col-md-12">
<form class="form-horizontal col-md-6" method="post" align="center">
<fieldset>

<!-- Form Name -->
<legend>Spherical Adiabatic Shock Radius Calculator</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="eRelease">Energy Release</label>  
  <div class="col-md-4">
  <input id="eRelease" name="eRelease" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $er;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="time">Time</label>  
  <div class="col-md-4">
  <input id="time" name="time" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $time;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="duMedium">Density of Undisturbed Medium</label>  
  <div class="col-md-4">
  <input id="duMedium" name="duMedium" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $du;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="sRadius">Shock Radius</label>  
  <div class="col-md-4">
  <input id="sRadius" name="sRadius" type="text" placeholder="Shock Radius is..." class="form-control input-md" required="" value="<?php echo $result;?>">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-2">
    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
  </div>
    <div class="col-md-1">
    <button id="reset" name="reset" class="btn btn-primary">Reset</button>
  </div>
</div>

</fieldset>
</form>
                      <!--Dynamically Generated Example-->
<div class="col-md-6" id="dynamicExample">
<?php   
    echo "<h4>Dynamic Example</h4><br>";
    echo "Energy Release
 = E =  ".$er."<br><br>Time = t = ".$time."<br><br>Density of Undisturbed Medium = ρ<sub>o</sub> =  ".$du."<br><br><b>Step 1:</b> E x t<sup>2</sup> = ".$er." * ".$time."<sup>2</sup> = ".$mul."<br><br><b>Step 2:</b> r ≈ (Et2 / ρ<sub>o</sub>)<sup>1/5</sup> = (".$mul." / ".$du.")<sup>1/5</sup> = ".$result;       
?>        
    
    </div>    
    </div>
    
    </div>            
        
        <!--Static Examples -->
<div class="col-md-12">
<div class="col-md-4"  style="border-style:solid;">
    <h4>Formula:</h4><br>
    r ≈ (Et<sup>2</sup> / ρ<sub>o</sub>)<sup>1/5</sup><br>
<p><br>Where,<br>
r = Shock Radius<br>
E = Energy Release<br>
t = Time<br>
ρ<sub>o</sub> = Density of Undisturbed Medium</p><br>
        </div>        
<div class="col-md-4">
<div style="border-style:solid;">
<h4>Example 1:</h4>
    <br>
    <p>Energy Release = E = 4</p>
    <p>Time = t = 2</p>
    <p>Density of Undisturbed Medium = ρ<sub>o</sub> = 3</p>
    <p><b>Step 1:</b>E x t<sup>2</sup> = 4 * 2<sup>2</sup> = 16</p>
    <p><b>Step 2:</b> (Et<sup>2</sup> / ρ<sub>o</sub>)<sup>1/5</sup> = (16 / 3)<sup>1/5</sup>= 1.3976542375432</p><br>
    <p>Shock Radius = 1.3976542375432</p>
</div>
</div>
    
    <!-- Second Example -->
    
    <div class="col-md-4">
    
<div  style="border-style:solid;">
<h4>Example 2:</h4>
    <br>
    <p>Energy Release = E = 6</p>
    <p>Time = t = 7</p>
    <p>Density of Undisturbed Medium = ρ<sub>o</sub> = 8</p>
    <p><b>Step 1:</b>E x t<sup>2</sup> = 6 * 7<sup>2</sup> = 294</p>
    <p><b>Step 2:</b> (Et<sup>2</sup> / ρ<sub>o</sub>)<sup>1/5</sup> = (294 / 8)<sup>1/5</sup>= 1.3976542375432</p><br>
    <p>Shock Radius =  2.0561342561231</p>

</div>
</div>      
</body>
</html>
