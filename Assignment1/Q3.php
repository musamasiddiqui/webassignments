<?php
        $mass = 0;
        $grav = 0;
        $fluid = 0;
        $obj = 0;
        $co = 0;
        $result = 0;
        $mul = 0;
        $mull = 0;
        $div = 0;
    if(isset($_POST['submit'])){
        $mass = $_POST['mass'];
        $grav = $_POST['gravity'];
        $fluid = $_POST['fluid'];
        $obj = $_POST['object'];
        $co = $_POST['coefficient'];
        $mul = $fluid*$obj*$co;
        $mull = (2*$mass*$grav);
        $div = $mull/$mul;
        $result = sqrt($div);
    }

?>
<html>
<head>
    <title>Q3</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script></head>
    <body>
        <div class="col-md-12">
<form class="form-horizontal col-md-6" align="center" method="post">
<fieldset>

<!-- Form Name -->
<legend>Terminal Velocity Calculator</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="mass">Mass of the Falling Object</label>  
  <div class="col-md-4">
  <input id="mass" name="mass" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $mass;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="gravity">Acceleration Due to Gravity</label>  
  <div class="col-md-4">
  <input id="gravity" name="gravity" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $grav;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="fluid">Density of Fluid</label>  
  <div class="col-md-4">
  <input id="fluid" name="fluid" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $fluid;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="object">Projected Area of the Object</label>  
  <div class="col-md-4">
  <input id="object" name="object" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $obj;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="coefficient">Drag Coefficient</label>  
  <div class="col-md-4">
  <input id="coefficient" name="coefficient" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $co;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tVelocity">Terminal Velocity</label>  
  <div class="col-md-4">
  <input id="tVelocity" name="tVelocity" type="text" placeholder="Terminal Velocity is..." class="form-control input-md" value="<?php echo $result;?>">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-2">
    <button id="submit" name="submit" class="btn btn-primary">Calculate</button>
  </div>
    <div class="col-md-1">
    <button id="reset" name="reset" class="btn btn-primary">Reset</button>
  </div>
</div>

</fieldset>
        
</form>
              <!--Dynamically Generated Example-->
<div class="col-md-6">
<?php   
    echo "<h4>Dynamic Example</h4><br>";
    echo " Mass of the Falling Object = m =  ".$mass."<br><br>Acceleration due to Gravity = g = ".$grav."<br><br>Density of Fluid = ρ = ".$fluid."<br><br>Projected Area of the Object = A = ".$obj."<br><br>Drag Coefficient: C<sub>d</sub> = ".$co."<br><br><b>Step 1:</b>(2 * m * g) = 2 x ".$mass." x ".$grav." = ".$mull."<br><br><b>Step 2:</b>(ρ x A x C<sub>d</sub>) = ".$fluid." x ".$obj." x ".$co." x  = ".$mul."<br><br><b>Step 3</b>:".$mull." / ".$mul." = ".$div."<br><br><b>Step 4:</b> Square Root:Sqrt(".$div.") = ".$result;       
?>        
    
    </div>    
    </div>
    </div>            
        
        <!--Static Examples -->
<div class="col-md-12">
<div class="col-md-4"  style="border-style:solid;">
    <h4>Formula:</h4><br>
    Vt = √ ((2 x m x g) / (ρ x A x Cd))
<br>
<p><br>Where,<br>
Vt = Terminal Velocity (Maximum Falling Speed)
<br>
m = Mass of the Falling Object<br>
g = Acceleration due to Gravity<br>
ρ = Density of Fluid<br>
A = Projected Area of the Object<br>
C<sub>d</sub> = Drag Coefficient</p><br>
        </div>        
<div class="col-md-4">
<div style="border-style:solid;">
<h4>Example 1:</h4>
    <br>
    <p>Mass of the Falling Object = m = 4
</p>
    <p>Acceleration due to Gravity = g = 2

</p>
    <p>Density of Fluid = ρ = 5
</p>
    <p>Projected Area of the Object = A = 2
</p>
    <p>Drag Coefficient: Cd = 1</p>

    <p><b>Step 1:</b>(2 * m * g) = 2 x 4 x 2 = 16</p>
    <p><b>Step 2:</b> (ρ x A x C<sub>d</sub>) = 5 x 2 x 1 x = 10 </p><br>
    <p><b>Step 3:</b>16 / 10 = 1.6</p>
    <p><b>Step 4</b>: Square Root:Sqrt(1.6) = 1.2649110640674</p>

</div>
</div>
    
    <!-- Second Example -->
    
    <div class="col-md-4">
    
<div  style="border-style:solid;">
<h4>Example 2:</h4>
     <br>
    <p>Mass of the Falling Object = m = 7
</p>
    <p>Acceleration due to Gravity = g = 5

</p>
    <p>Density of Fluid = ρ = 2
</p>
    <p>Projected Area of the Object = A = 5
</p>
    <p>Drag Coefficient: Cd = 2</p>

    <p><b>Step 1:</b>(2 * m * g) =  2 x 7 x 5 = 70</p>
    <p><b>Step 2:</b> (ρ x A x C<sub>d</sub>) =  2 x 5 x 2 x = 20 </p><br>
    <p><b>Step 3:</b>70 / 20 = 3.5</p>
    <p><b>Step 4</b>: Square Root:Sqrt(3.5) = 1.870828693387</p>

</div>
</div>
</body>
</html>
