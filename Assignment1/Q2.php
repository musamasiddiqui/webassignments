<?php
    $sm = 0;
    $mul = 0;
    $cs = 0;
    $wt = 0;
    $result = 0;
    if(isset($_POST['submit'])){
        $sm = $_POST['sModulus'];
        $cs = $_POST['csWidth'];
        $wt = $_POST['wThickness'];
        $mul = ($sm*$cs)*($wt*$wt*$wt);
        $result = ((1/3)*$mul);
    }

?>
<html>
<head>
    <title>Q2</title>
    <script type="text/javascript">
        var x = document.getElementById("dynamicExample");
        x.style.visibility="hidden";</script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script></head>
    <body>
        <div class="col-md-12">
<form class="form-horizontal col-md-6" align="center" method="post">
<fieldset>

<!-- Form Name -->
<legend>Calculate Torsional Stiffness of Long Flat Ribbon</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="sModulus">Shear Modulus</label>  
  <div class="col-md-4">
  <input id="sModulus" name="sModulus" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $sm?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="csWidth">Cross Sectional Width</label>  
  <div class="col-md-4">
  <input id="csWidth" name="csWidth" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $cs?>"></div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="wThickness">Wall Thickness</label>  
  <div class="col-md-4">
  <input id="wThickness" name="wThickness" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $wt?>"></div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tRigidity">Torsional Rigidity</label>  
  <div class="col-md-4">
  <input id="tRigidity" name="tRigidity" type="text" placeholder="Result is..." class="form-control input-md" value="<?php echo $result;?>">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-2">
    <button id="submit" name="submit" class="btn btn-primary" onclick="myFunc()">Calculate</button>
  </div>
    <div class="col-md-2">
    <button id="reset" name="reset" class="btn btn-primary">Reset</button>
  </div>
</div>

</fieldset>
</form>
              <!--Dynamically Generated Example-->
<div class="col-md-6" id="dynamicExample">
<?php   
    echo "<h4>Dynamic Example</h4><br>";
    echo "Shear Modulus = μ =  ".$sm."<br><br>Cross Sectional Width = w = ".$cs."<br><br>Wall Thickness =  ".$wt."<br><br><b>Step 1:</b> μ*wt<sup>3</sup> = ".$sm." * ".$cs."<sup>3</sup> = ".$mul."<br><br><b>Step 2:</b> C = (1/3) * (μwt3) = 1/3 * (".$mul.") = ".$result;       
?>        
    
    </div>    
    </div>
    
    </div>            
        
        <!--Static Examples -->
<div class="col-md-12">
<div class="col-md-4"  style="border-style:solid;">
    <h4>Formula:</h4><br>
    C = (1/3) * (μwt<sup>3</sup>)<br>
<p><br>Where,<br>
C = Torsional Rigidity or Stiffness<br>
μ = Shear Modulus<br>
w = Cross Sectional Width<br>
t = Wall Thickness</p><br>
        </div>        
<div class="col-md-4">
<div style="border-style:solid;">
<h4>Example 1:</h4>
    <br>
    <p>Shear Modulus = μ = 3</p>
    <p>Cross Sectional Width = w = 4
</p>
    <p>Wall Thickness = 2</p>
    <p><b>Step 1:</b> μ*wt<sup>3</sup> = 3 * 4<sup>3</sup> = 96</p>
    <p><b>Step 2:</b> C = (1/3) * (μwt<sup>3</sup>) = 1/3 * (96) = 32 </p><br>
    <p>Torsional Rigidity
 = 32</p>
</div>
</div>
    
    <!-- Second Example -->
    
    <div class="col-md-4">
    
<div  style="border-style:solid;">
<h4>Example 2:</h4>
       <br>
    <p>Shear Modulus = μ = 5</p>
    <p>Cross Sectional Width = w = 45
</p>
    <p>Wall Thickness = 5</p>
    <p><b>Step 1:</b> μ*wt<sup>3</sup> = 5 * 5<sup>3</sup> = 3125</p>
    <p><b>Step 2:</b> C = (1/3) * (μwt<sup>3</sup>) = 1/3 * (3125) = 1041.6666666667 </p><br>
    <p>Torsional Rigidity
 = 1041.6666666667</p>
</div>
</div>
<script>
function myFunction() {
    var x = document.getElementById("dynamicExample");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>
      
</body>
</html>
