<?php
        $wn = 0;
        $radius = 0;
        $yModulus = 0;
        $density = 0;
        $result = 0;
        $div = 0;
    if(isset($_POST['submit'])){
        $wn = $_POST['wNumber'];
        $radius = $_POST['rRadius'];
        $yModulus = $_POST['yModulus'];
        $density = $_POST['density'];
        $ep = $yModulus/$density;
        $div = ($wn*$radius)/2;
        $result = $div*(pow($ep,1/2));
    }

?>
<html>
<head>
    <title>Q5</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script></head>
    <body>
        <div class="col-md-12">
<form class="form-horizontal col-md-6" align="center" method="post">
<fieldset>

<!-- Form Name -->
<legend>Speed of Transverse Wave in a Thin Circular Rod Calculator</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="wNumber">Wave Number</label>  
  <div class="col-md-4">
  <input id="wNumber" name="wNumber" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $wn;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="rRadius">Rod Radius</label>  
  <div class="col-md-4">
  <input id="rRadius" name="rRadius" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $radius;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="yModulus">Young Modulus</label>  
  <div class="col-md-4">
  <input id="yModulus" name="yModulus" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $yModulus;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="density">Density</label>  
  <div class="col-md-4">
  <input id="density" name="density" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $density;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tWave">Speed of Transverse Wave</label>  
  <div class="col-md-4">
  <input id="tWave" name="tWave" type="text" placeholder="Speed of Transverse Wave is..." class="form-control input-md" required="" value="<?php echo $result;?>">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-2">
    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
  </div>
    <div class="col-md-1">
    <button id="reset" name="reset" class="btn btn-primary">Reset</button>
  </div>
</div>

</fieldset>
</form>
           <!--Dynamically Generated Example-->
<div class="col-md-6">
<?php   
    echo "<h4>Dynamic Example</h4><br>";
    echo "Wave Number = k = ".$wn."<br><br>Rod Radius = a =  ".$radius."<br><br>Young Modulus = E = ".$yModulus."<br><br>Density = ρ = 
".$density."<br><br><b>Step 1:</b> (ka / 2 ) = ".$wn." x "."$radius"." / 2 = ".$div."<br><br><b>Step 2:</b> [E/p]<sup>1/2</sup> = [".$yModulus." / ".$density." ]<sup>1/2</sup> = ".$result;
?>        
    
    </div>    
    </div>
    
    </div>            
        
        <!--Static Examples -->
<div class="col-md-12">
<div class="col-md-4"  style="border-style:solid;">
    <h4>Formula:</h4><br>
    vt = (ka / 2) [E / ρ]<sup>1/2</sup><br>
<p>Where,<br>
v<sub>t</sub> = Speed of Transverse Wave<br>
k = Wave Number<br>
a = Rod Radius<br>
E = Young Modulus<br>
    ρ = Density</p>
    
        </div>        
<div class="col-md-4">
<div style="border-style:solid;">
<h4>Example 1:</h4>
    <br>
    <p>Wave Number = k = 4</p>
    <p>Rod Radius = a = 2</p>
    <p>Young Modulus = E = 4</p>
    <p>Density = ρ = 2</p>
    <p>Step 1: (ka / 2 ) = 4 x 2 / 2 = 4 </p>
    <p>Step 2: [E/p]<sup>1/2</sup> = [4 / 2 ]<sup>1/2</sup> = 5.6568542494924 </p>
    <p><b>Speed of Transverse Wave</b> = 5.6568542494924</p>
</div>
</div>
    
    <!-- Second Example -->
    
    <div class="col-md-4">
    
<div  style="border-style:solid;">
<h4>Example 2:</h4>
        <br>
    <p>Wave Number = k = 6</p>
    <p>Rod Radius = a = 8</p>
    <p>Young Modulus = E = 4</p>
    <p>Density = ρ = 3</p>
    <p>Step 1: (ka / 2 ) = 6 x 8 / 2 = 24 </p>
    <p>Step 2: [E/p]<sup>1/2</sup> = [4 / 3 ]<sup>1/2</sup> = 27.712812921102 </p>
    <p><b>Speed of Transverse Wave</b> = 27.712812921102</p></div>
</div>

        
    
</body>
</html>
