 <?php
    $result =0;
    $tr = 0;
    $ta = 0;
    $rl = 0;
    $div = 0;
    if(isset($_POST['submit'])){
        $tr = $_POST['tRigidity'];
        $ta = $_POST['tAngle'];
        $rl = $_POST['rLength'];
        $dd = $_POST['selectbasic'];
        $div = $ta/$rl; 
        $conv = $rl/$dd;
        $result = ($tr*($ta/$conv))/10;
        //display($tr,$ta,$rl,$result);
        //echo "Value of Torsional Rigidy: ".$tr;
        //echo "<br>Value of Twist Angle: ".$ta;
        //echo "<br>Value of Rod Length: ".$rl;
        //echo "<br>Result is: ".$tr;
    }

?>
<html>
<head>
    <title>Q1</title>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script></head>
    <body>
        <div class="col-md-12">        

<form class="form-horizontal col-md-6" align="center" method="post">
<fieldset>

<!-- Form Name -->
<legend>Twisting Couple on a Homogenous Rod Calculator</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tRigidity">Torsional Rigidity</label>  
  <div class="col-md-4">
  <input id="tRigidity" name="tRigidity" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $tr;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tAngle">Twist Angle</label>  
  <div class="col-md-4">
  <input id="tAngle" name="tAngle" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $ta;?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="rLength">Rod Length</label>  
  <div class="col-md-4">
  <input id="rLength" name="rLength" type="text" placeholder="Enter Number..." class="form-control input-md" required="" value="<?php echo $rl;?>">
    
  </div>
    <div class="col-md-2">
    <select id="selectbasic" name="selectbasic" class="form-control">
      <option value="10">mm</option>
      <option value="1">cm</option>
      <option value="0.1">dm</option>
      <option value="0.01">m</option>
      <option value="0.00001">km</option>
      <option value="0.000006213711922373">mi</option>
      <option value="0.3937007874016">in</option>
      <option value="0.03280839895013">ft</option>
      <option value="0.01093613298338">yd</option>
    </select>
    
    
    </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tCouple">Twisting Couple</label>  
  <div class="col-md-4">
  <input id="tCouple" name="tCouple" type="text" placeholder="Result is..." class="form-control input-md" value="<?php echo $result;?>">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-2">
    <button id="submit" name="submit" class="btn btn-primary">Calculate</button>
  </div>
     <div class="col-md-2">
    <button id="reset" name="reset" class="btn btn-primary">Reset</button>
  </div>
</div>

</fieldset>
</form>
            <!--Dynamically Generated Example-->
<div class="col-md-6">
<?php   
    echo "<h4>Dynamic Example</h4><br>";
    echo "Input Value for Torsional Rigidity: ".$tr."<br><br>Input Value for Twist Angle:".$ta."<br><br>Input Value for Rod Length: ".$rl."<br><br><b>Step 1:</b> Φ / | = ".$ta." / ".$rl." = ".$div."<br><br><b>Step 2:</b> C*(Φ / |) = ".$tr." * (".$div.") = ".$result;       
?>        
    
    </div>    
    </div>
    
    </div>            
        
        <!--Static Examples -->
<div class="col-md-12">
<div class="col-md-4"  style="border-style:solid;">
    <h4>Formula:</h4><br>
    G = C*(Φ / l)<br>
<p>Where,<br>
G = Twisting Couple<br>
C = Torsional Rigidity<br>
Φ = Twist Angle<br>
l = Rod Length</p><br>
        </div>        
<div class="col-md-4">
<div style="border-style:solid;">
<h4>Example 1:</h4>
    <br>
    <p>Torsional Rigidity = C = 4</p>
    <p>Twist Angle = Φ = 2</p>
    <p>Rod Length = | = 4</p>
    <p>Twisting Couple = G = ?</p>
    <p>Step 1: Φ / | = 2 * 4 = 0.5 </p>
    <p>Step 2: C*(Φ / |) = 4 * 0.5 = 2 </p><br>
    <p>Twisting Couple = 2</p>
</div>
</div>
    
    <!-- Second Example -->
    
    <div class="col-md-4">
    
<div  style="border-style:solid;">
<h4>Example 2:</h4>
    <br>
    <p>Torsional Rigidity = C = 5</p>
    <p>Twist Angle = Φ = 5</p>
    <p>Rod Length = | = 5</p>
    <p>Twisting Couple = G = ?</p>
    <p>Step 1: Φ / | = 5 * 5 = 1 </p>
    <p>Step 2: C*(Φ / |) = 5 * 1 = 5 </p><br>
    <p>Twisting Couple = 5</p>
</div>
</div>

        
</body>
</html>
